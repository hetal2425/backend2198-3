﻿using EchoChat.Models;
using EchoChat.Repository;
using System;

namespace EchoChat.ServiceProvider
{
    public class WebsiteProvider : IWebsiteProvider
    {
        IWebsiteRepository websiteRepository;
        public WebsiteProvider(IWebsiteRepository oWebsiteRepository)
        {
            websiteRepository = oWebsiteRepository;
        }

        public bool SaveDemoRequest(DemoRequestDTO demo)
        {
            return websiteRepository.SaveDemoRequest(demo);
        }
    }
}
