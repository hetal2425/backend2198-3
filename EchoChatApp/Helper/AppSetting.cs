﻿using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EchoChatApp.Helper
{
    public class AppSetting : IAppSetting
    {
        public static List<AppSettingsVM> appSettings = new List<AppSettingsVM>();
        public static DateTime LastUpdated = DateTime.MinValue;

        private static IClientProvider clientProvider;
        private ILog logger;
        public AppSetting(IClientProvider oClientProvider)
        {
            clientProvider = oClientProvider;
            logger = Logger.GetLogger(typeof(AppSetting));
        }
        public List<AppSettingsVM> FillAndGetAppSettings(int entityID)
        {
            if (true)//(!AppSetting.appSettings.Any(a => a.EntityID.Equals(entityID)) || AppSetting.LastUpdated.AddDays(1) <= DateTime.Now)
            {
                List<AppSettingsVM> settings = clientProvider.GetAppSettingsByEntityID(entityID);
                if (settings != null && settings.Count > 0)
                {
                    appSettings.AddRange(settings);
                    LastUpdated = DateTime.Now;
                }
                else
                {
                    settings = clientProvider.GetAppSettingsByEntityID(0);
                }

                return settings;
            }
            //else
            //{
            //    List<AppSettingsVM> UserDetails = AppSetting.appSettings.Where(a => a.EntityID.Equals(entityID)).ToList();
            //    if (UserDetails != null && UserDetails.Count > 0)
            //    {
            //        return AppSetting.appSettings.Where(a => a.EntityID.Equals(entityID)).ToList();
            //    }
            //    else
            //    {
            //        return AppSetting.appSettings.Where(a => a.EntityID.Equals(0)).ToList();
            //    }
            //}
        }
    }
}