﻿using EchoChat.Channel;
using EchoChat.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace EchoChat.ChannelConnector
{
    public class Connector : IConnector
    {
        IBroadcaster broadcaster;
        public Connector(IBroadcaster oBroadcaster)
        {
            broadcaster = oBroadcaster;
        }

        public ConcurrentBag<ChannelOutput> CallBroadcaster(MessageDetails messageDetails, List<string> channels, List<UserChannelData> userChannelDatas, List<AppSettingsVM> settings)
        {
            try
            {
                List<ChannelLevelSettings> channelLevelSettings = settings.Select(s => new ChannelLevelSettings() { Key = s.Key, Value = s.Value, Details = s.Details, Value1 = s.Value1, Value2 = s.Value2, Value3 = s.Value3, Value4 = s.Value4, Value5 = s.Value5, Value6 = s.Value6, Value7 = s.Value7 }).ToList();
                return broadcaster.Process(messageDetails, channels, userChannelDatas, channelLevelSettings);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
