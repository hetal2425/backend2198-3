﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class NoticeList
    {
        public string NoticeTitle { get; set; }
        public string NoticeDetail { get; set; }
        public string NoticeDate { get; set; }
        public string FirstName { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public int ReadCount { get; set; }
        public int ReplyCount { get; set; }
        public int MemberCount { get; set; }
        public int NoticeID { get; set; }
    }
}
