﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Amazon;
using Amazon.SimpleEmail;
using EchoChat.ChannelModels;
using Newtonsoft.Json;

namespace EchoChat.ChannelVerification
{
    public static class EmailVerification
    {
        public static bool VerifyEmailID(string emailID)
        {

            try
            {
                string apiKey = "ebde3133bedadee8efadadbf617d8ceed83ec32b4881d8ae36b399ff430f";                
                string responseString = "";
                string apiURL = "http://api.quickemailverification.com/v1/verify?email=" + HttpUtility.UrlEncode(emailID) + "&apikey=" + apiKey;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiURL);
                request.Method = "GET";

                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader ostream = new StreamReader(response.GetResponseStream()))
                    {
                        responseString = ostream.ReadToEnd();
                        EmailVerificationModel verification = JsonConvert.DeserializeObject<EmailVerificationModel>(responseString);
                        return (verification.success == "true") ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class EmailVerificationModel
    {
        public string result { get; set; }
        public string reason { get; set; }
        public string disposable { get; set; }
        public string accept_all { get; set; }
        public string role { get; set; }
        public string free { get; set; }
        public string email { get; set; }
        public string user { get; set; }
        public string domain { get; set; }
        public string mx_record { get; set; }
        public string mx_domain { get; set; }
        public string safe_to_send { get; set; }
        public string did_you_mean { get; set; }
        public string success { get; set; }
        public string message { get; set; }
    }
}
