﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public interface IGroupRepository
    {
        GroupDTO GetGroupByID(int groupID);
        UserGroupModel GetUserGroups(Guid userID, string freeText, int pageSize, int PageNo, int timeZoneOffset);
        GroupDTO CreateUpdateGroup(GroupDTO group);
        bool DeleteGroup(int groupID);
        ListOrGroupWiseUsers GetMembersByGroupID(int groupID, int pageSize, int PageNo, string freeText, int timeZoneOffset);
        ListOrGroupWiseUsers GetMembersByListID(int listID, int pageSize, int PageNo, string freeText, int timeZoneOffset);
        UserChannelDataWithGroup GetGroupMappingUsers(string groupID, int parentID, string noticeDetails);
        List<UserChannelInfo> GetUsersForPendingNotice(int noticeID, bool isSMS, bool isEmail, bool isWA);
        bool CreateUpdateList(ListDTO list);
        bool DeleteList(int listID);
        bool SetGroupListMapping(GroupListMapping mapping);
        List<ListDTO> GetAllListByClientID(int clientID, string freeText, int timeZoneOffset);
        List<GroupDTO> GetAllGroupsByClientID(int clientID);
        List<ListSmallDTO> GetAllListByGroupID(int groupID);
        bool CheckGroupAlreadyExists(int groupID, string name);
        bool CheckListAlreadyExists(int listID, string name);
        GroupDTO CreateInteGrationGroup(GroupDTO group, List<Guid> lstGUsers);
        int CheckIntegrationGroupAlreadyExists(string name);
        GroupDTO CreatePrivateGroup(GroupDTO group, List<Guid> lstGUsers);
        int CheckPrivateGroupAlreadyExists(string name, string mobileNo);
    }
}
