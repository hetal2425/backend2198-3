﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EchoChat.Channel.Factory;
using EchoChat.Constants;

namespace EchoChat.Channel
{
    public class ViberChannel : BaseChannel
    {
        public string channelToken { get; set; }
        ViberChannel()
        {
            channelToken = GetToken();
        }

        public string GetToken()
        {
            return string.Empty;
        }

        public override Task Process()
        {
            DelayTesting(ChannelMaster.ViberChannel, string.Empty);
            return Task.Delay(0);
        }
    }
}
