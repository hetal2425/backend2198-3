﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EchoChat.Channel.Factory;
using EchoChat.Constants;

namespace EchoChat.Channel
{
    public class TelegramChannel : BaseChannel
    {
        public string channelToken { get; set; }
        TelegramChannel()
        {
            channelToken = GetToken();
        }

        public string GetToken()
        {
            return string.Empty;
        }

        public override Task Process()
        {
            DelayTesting(ChannelMaster.WhatsappChannel, string.Empty);
            return Task.Delay(0);
        }
    }
}
